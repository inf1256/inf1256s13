package inf1256s13;

public class Etudiant  extends Citoyen{
    private String universite = "";
    public Etudiant(String nomEtudiant, String prenomEtudiant){//constructeur
        this.setNom(nomEtudiant);
        this.setPrenom(prenomEtudiant);
    }
    public String getUniversite(){
        return this.universite;
    }
    protected void setUniversite(String uni){
        this.universite = uni;
    }
    public void afficherInfo(){
        System.out.println("Voici les informations de l'étudiant-e");
        System.out.println("Nom: "+this.getNom());
        System.out.println("Prénom: "+this.getPrenom());
        System.out.println("Université: "+this.getUniversite());
    }
}
