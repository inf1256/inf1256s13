/**
 * 
 */
package inf1256s13;

/**
 * @author johnny Tsheke
 *
 */
public interface PersonneInterface2 {
     String COULEUR_SANG = "ROUGE";//sera public et final pcq dans interface
     String getNom(); //signature de la methode
     void setNom(String no);
     String getPreNom();
     void setPreNom(String pre);
     default void afficherNomComplet(){ //méthode par défaut
          System.out.println("Nom complet: "+ this.getNom() +", "+ this.getPreNom());
     }
}
