package inf1256s13;

public abstract class Personne {
	public static String COULEUR_SANG = "ROUGE";
	private String genre; //pas une constante et private ok.
    abstract String getNom(); //signature de la methode
    abstract void setNom(String no);
    abstract String getPreNom();
    abstract void setPreNom(String pre);
    public String getGenre(){//implémentation
    	return(this.genre);
    }
    
    public void setGenre(String ge){//implémentation
    	this.genre = ge;
    }
}
