/**
 * 
 */
package inf1256s13;

/**
 * @author johnny Tsheke
 *
 */
public interface PersonneInterface {
     String COULEUR_SANG = "ROUGE";//sera public et final pcq dans Interface
     // private String COULEUR_SANG = "ROUGE";// erreur à cause du mot private
     String getNom(); //signature de la methode
     void setNom(String no);
     String getPreNom();
     void setPreNom(String pre);

}
