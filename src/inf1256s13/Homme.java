/**
 * 
 */
package inf1256s13;

/**
 * @author Johnny Tsheke
 *
 */
public class Homme implements PersonneInterface {
	private String nom="";
	private String prenom="";

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nomPers) {
		this.nom = nomPers;
	}

	public String getPreNom() {
		return this.prenom;
	}

	public void setPreNom(String prenomPers) {
		this.prenom = prenomPers;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
}
