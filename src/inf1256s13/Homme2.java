/**
 * 
 */
package inf1256s13;

/**
 * @author Johnny Tsheke
 *
 */
public class Homme2 implements PersonneInterface2 {
	private String nom="";
	private String prenom="";

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nomPers) {
		this.nom = nomPers;
	}

	public String getPreNom() {
		return this.prenom;
	}

	public void setPreNom(String prenomPers) {
		this.prenom = prenomPers;
	}

	public static void main(String[] args) {
		Homme2 homme2 = new Homme2();
		homme2.setNom("Tremblay");
		homme2.setPreNom("Jean");
		homme2.afficherNomComplet(); //appel méthode par défaut
	}
}
